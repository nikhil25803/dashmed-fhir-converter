import uuid

from fhir.resources.bundle import Bundle  # type: ignore
from fhir.resources.bundle import BundleEntry  # type: ignore
from fhir.resources.coding import Coding  # type: ignore
from fhir.resources.composition import Composition  # type: ignore
from fhir.resources.composition import CompositionSection  # type: ignore
from fhir.resources.codeableconcept import CodeableConcept  # type: ignore
from fhir.resources.encounter import Encounter  # type: ignore
from fhir.resources.reference import Reference  # type: ignore
from common import (
    create_section,
    create_condition_construct,
    create_procedure_construct,
    create_medication_construct,
)

from fhir_converter.common import get_patient_construct, get_practitioner_construct


# def create_section(title, code, display, text):
#     ref_id = str(uuid.uuid4())
#     section = CompositionSection.construct(
#         id=ref_id,
#         title=title,
#         code=CodeableConcept.construct(
#             text=display,
#             coding=Coding.construct(
#                 system="http://snomed.info/sct",
#                 code=code,
#                 display=display
#             )
#         ),
#         text=text,

#     )
#     return section


def create_opconsult_record(input_json):
    patient_info = input_json.get("patient", {})

    patient = get_patient_construct(patient_info)
    patient_ref = Reference.construct(
        reference=f"urn:uuid:{patient.id}", display=patient.name[0]["text"]
    )

    practitioner_info = input_json.get("practitioner", {})
    practitioner = get_practitioner_construct(practitioner_info)
    practitioner_ref = Reference.construct(
        reference=f"urn:uuid:{practitioner.id}", display=practitioner.name[0]["text"]
    )

    encounter = Encounter.construct(
        id=str(uuid.uuid4()),
        status="finished",
        subject=patient_ref,
        class_fhir=Coding.construct(
            system="http://terminology.hl7.org/CodeSystem/v3-ActCode",
            code="IMP",
            display="inpatient encounter",
        ),
    )
    encounter_ref = Reference.construct(
        reference=f"urn:uuid:{encounter.id}", display="Encounter/OP Consult Record"
    )

    ref_data = [patient, practitioner, encounter]

    section_details = {
        "Chief Complaints": ("422843007", "Chief complaint section"),
        # "Physical Examination": ("422843007", "Physical Examination section"),
        # "Allergies": ("722446000", "Allergy record"),
        # "Medical History": ("371529009", "History and physical report"),
        # "Family History": ("422432008", "Family history section"),
        # "Investigation Advice": ("721963009", "Order document"),
        # "Medications": ("721912009", "Medication summary document"),
        # "Follow Up": ("390906007", "Follow-up encounter"),
        # "Procedure": ("371525003", "Procedure report"),
        # "Referral": ("306206005", "Clinical procedure report"),
        # "Other Observations": ("404684003", "Clinical finding"),
        # "Document Reference": ("371530004", "Clinical consultation report"),
    }

    # sections = [
    #     create_section(title, *details, input_json.get(title.replace(" ", ""), "")) for title, details in
    #     section_details.items()
    # ]

    """
    Working on `ChiefComplaints` part
    """

    sections = []
    # Chief Complaint Section
    complaints = input_json["ChiefComplaints"]

    bundle_construct_condition = []
    condition_references = []

    for _comp in complaints:
        id = str(uuid.uuid4())

        _comp_condition = create_condition_construct(
            text=_comp, patient_ref=patient_ref, id=id
        )

        condition_references.append({"reference": id, "display": _comp})

        bundle_construct_condition.append(_comp_condition)

    conditions = create_section(
        title="Chief complaints",
        code="422843007",
        display="Chief complaint section",
        text="Chief complaint section",
        references=condition_references,
    )
    sections.append(conditions)

    ref_data.extend(bundle_construct_condition)

    # section_refs = [Reference.construct(reference=f"urn:uuid:{section.id}", display=f"{section.title}") for section in sections]

    """
    Working On `Procedure` Part
    """
    procedures = input_json["Procedure"]

    bundle_construct_procedure = []
    procedure_references = []

    for procedure in procedures:
        id = str(uuid.uuid4())
        procedure_references.append({"reference": id, "display": procedure})

        procedure_construct = create_procedure_construct(
            id=id, text=procedure, patient_ref=patient_ref
        )
        bundle_construct_procedure.append(procedure_construct)

    procedure_section = create_section(
        title="Procedure",
        code="371525003",
        display="Procedure report.",
        text="Procedure report",
        references=procedure_references,
    )

    sections.append(procedure_section)
    ref_data.extend(bundle_construct_procedure)

    """
    Working on investigation advice part
    """
    investigation_advices = input_json["InvestigationAdvice"]
    investigation_references = []

    for investigation in investigation_advices:
        id = str(uuid.uuid4())
        investigation_references.append({"reference": id, "display": investigation})

    investigation_section = create_section(
        title="Investigation Advice",
        code="721963009",
        display="Order document",
        text="Order document",
        references=investigation_references,
    )

    sections.append(investigation_section)

    """
    Working on Other Observations part
    """

    other_observations = input_json["OtherObservations"]
    observations_references = []
    id = str(uuid.uuid4())
    observations_references.append({"reference": id, "display": other_observations})

    onservations_section = create_section(
        title="Other Observations",
        code="404684003",
        display="Clinical finding",
        text="Clinical finding",
        references=observations_references,
    )

    sections.append(onservations_section)

    """
    Working on Medications Part
    """
    medications_listed = input_json["Medications"]
    medications_references = []
    bundled_construct_medications = []

    for medication in medications_listed:
        id = str(uuid.uuid4())
        medications_references.append({"reference": id, "display": "MedicationRequest"})

        request_statemet = f"{medication['quantity']} dosage every {medication['frequency']} for {medication['noOfDays']} days."

        medication_construct = create_medication_construct(
            id=id,
            medicine_name=medication["name"],
            text=request_statemet,
            date=input_json["date"],
            patient=patient_ref,
            practitioner=practitioner_ref,
        )

        bundled_construct_medications.append(medication_construct)

    medications_section = create_section(
        title="Medications",
        code="721912009",
        display="Medication summary document",
        text="Medication summary document",
        references=medications_references,
    )

    sections.append(medications_section)
    ref_data.extend(bundled_construct_medications)

    """
    Create Composition resource for OP Consult Record
    """
    composition = Composition.construct(
        id=str(uuid.uuid4()),
        title="OP Consult Record",
        date=input_json["date"],
        status="final",  # final | amended | entered-in-error | preliminary,
        type=CodeableConcept.construct(
            text="OP Consult Record",
            coding=Coding.construct(
                system="http://snomed.info/sct",
                code="371530004",
                display="Clinical consultation report",
            ),
        ),
        encounter=encounter_ref,
        author=[practitioner_ref],
        subject=patient_ref,
        section=sections,
    )

    bundle = Bundle.construct()
    bundle.type = "collection"
    bundle.entry = [
        BundleEntry.construct(
            fullUrl=f"urn:uuid:{composition.id}", resource=composition
        )
    ]
    bundle.entry.extend(
        [
            BundleEntry.construct(fullUrl=f"urn:uuid:{ref.id}", resource=ref)
            for ref in ref_data
        ]
    )

    return bundle.json(indent=2)


if __name__ == "__main__":
    print("FHIR Bundle for OP Consult Record")
    input_json = {
        "patient": {
            "name": "Aman",
            "gender": "Male",
            "patient_id": "1234567890",
            "telecom": "1234567890",
        },
        "practitioner": {
            "name": "Aman Practitioner",
            "gender": "Female",
            "practitioner_id": "1234567890",
            "telecom": "1234567890",
        },
        "date": "2021-06-01",
        "ChiefComplaints": ["Bronchospasm", "Feeling Feverish"],
        "PhysicalExamination": {
            "Weight (kg)": 80,
            "Height (cm)": 120,
            "BMI": 55.56,
            "Respiratory rate": 23,
            "Temperature (C)": 36,
            "Location": None,
            "Pulse": 25,
            "O2 Saturation": 23,
            "Systolic blood pressure*": 26,
            "Diastolic blood pressure*": 25,
            "Blood Pressure Position": None,
            "Blood Pressure Source": None,
            "Head circumference": None,
            "Rh Factor": None,
            "LMP": None,
            "EDD": None,
        },
        "Allergies": "sample text",
        "MedicalHistory": "sample text",
        "FamilyHistory": "sample text",
        "InvestigationAdvice": [
            "Albumin, Fluid",
            "Basic Metabolic Panel",
            "Blood Urea Nitrogen",
            "Glucose",
            "Glucose Random",
            "Hemoglobin A1C",
            "Lipid Panel",
            "Urinalysis By Dip Stick Without Microscope",
            "X-Ray Bone Density BP",
        ],
        "Medications": [
            {
                "name": "Paracetamol 250 mg",
                "formulation": "ict-10",
                "frequency": "Every 30 min",
                "noOfDays": "7",
                "quantity": "2",
                "issueDate": "22/12/2014",
                "comment": "this is a comment",
            },
            {
                "name": "Azithromycin 250 mg",
                "formulation": "ict-10",
                "frequency": "Every 10 min",
                "noOfDays": "30",
                "quantity": "2",
                "issueDate": "22/12/2014",
                "comment": "this is a comment",
            },
        ],
        "FollowUp": "sample text",
        "Procedure": [
            "Administration of severe acute respiratory syndrome coronavirus 2 (SARS-CoV-2) vaccine",
            "Infection surveillance procedure",
        ],
        "Referral": "sample text",
        "OtherObservations": "Patient summaries play a crucial role in improving healthcare efficiency and the continuity of care, as they provide an accurate picture of who the patient is and how they have been doing",
        "DocumentReference": "sample text",
    }

    fhir_bundle = create_opconsult_record(input_json)
    with open("output.json", "w+") as json_file:
        json_file.write(fhir_bundle)
    # print(fhir_bundle)
